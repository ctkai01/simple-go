FROM golang:1.20.13-alpine3.18 AS builder

RUN mkdir /app
WORKDIR /app

COPY . .
RUN go mod tidy

RUN CGO_ENABLE=0 GOOS=linux go build -a -installsuffix cgo -o simple-go main.go

FROM alpine:3.18

RUN mkdir /app/
WORKDIR /app

COPY --from=builder /app/simple-go .

ENTRYPOINT [ "./simple-go" ]
