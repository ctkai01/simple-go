package main

import (
	"fmt"
	"os"
)

func main() {
	name := os.Getenv("NAME")
	if name == "" {
		name = "World"
	}

	message := fmt.Sprintf("Hello, %s!", name)
	fmt.Println(message)
}

